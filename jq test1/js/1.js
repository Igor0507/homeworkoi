const str = "Профессионал — не тот, , кто в совершенстве владеет техникой ремесла (с этим-то у него как раз полный порядок), а тот, кто, получив задание, всегда выдает конечный результат, невзирая ни на какие привходящие моменты… Ибо профессионал отличается от любителя еще и тем, что играет не до забитого им красивого гола и не до своего «психологического кризиса», а до шестидесятой секунды последней минуты матча.";
const substr = "профессионал";


function getSubstrCount(str, substr, flag) {

    let currstr = str;
    let currsubstr = substr;
    let fincounter = 0;
    if (!flag){
        currstr =  currstr.toLowerCase();
    }
    currstr = currstr.replace(/[\s-,()«»—]/g, ' ')
    console.log(currstr)
    let arr = currstr.split(" ")
    arr.forEach(function (e) {
        if(e == currsubstr){
            fincounter++;
        }
    })
    return fincounter;
}

console.log(getSubstrCount(str,substr, true));