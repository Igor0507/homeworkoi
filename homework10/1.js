const userPass = document.getElementById("input-password");
const checkPass = document.getElementById("check-password");
const inputImg = document.getElementById("pass-icon-in");
const checkImg = document.getElementById("pass-icon-check");
const confirmBtn = document.getElementById("confirm-btn");

inputImg.addEventListener("click",function () {
    switchVisual(inputImg,userPass);
});

checkImg.addEventListener("click", function () {
    switchVisual(checkImg,checkPass);
})

confirmBtn.addEventListener("click",function () {
    checkValue()
})

function checkValue() {
    if(userPass.value === checkPass.value){
        alert("You are welcome");
    }
    else{
        const errorMsg = document.createElement("p");
        errorMsg.textContent = "Нужно ввести одинаковые значения";
        errorMsg.style.color = "red";
        checkPass.after(errorMsg);
    }
}



function switchVisual(tempImg,tempPass) {
    if (tempImg.classList.contains("fa-eye")){
        tempImg.classList.remove("fa-eye");
        tempImg.classList.add("fa-eye-slash");
        tempPass.type = "text";
    }
    else{
        tempImg.classList.remove("fa-eye-slash");
        tempImg.classList.add("fa-eye");
        tempPass.type = "password";
    }
}

