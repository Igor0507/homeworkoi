let arr = ['hello', 'world', 23, '23', null];
let deletedType = "number";

function filterBy(tempArr, dataType) {
   for(let i = 0 ; i <= tempArr.length-1 ; i++) {
        switch (dataType) {
            case "number":
                if (typeof tempArr[i] === "number"){
                    tempArr.splice(i, 1);
                }
            case "boolean":
                if (typeof tempArr[i] === "boolean"){
                    tempArr.splice(i, 1);
                }

            case "null":
                if (typeof tempArr[i] === "object"){
                    tempArr.splice(i, 1);
                }

            case "undefined":
                if (typeof tempArr[i] === "undefined"){
                    tempArr.splice(i, 1);
                }

            case "string":
                if (typeof tempArr[i] === "string"){
                    tempArr.splice(i, 1);
                }
        }
    };
   return tempArr;
}

console.log(filterBy(arr, deletedType))