const userPrice = document.getElementById("user-price");
const priceWindow = document.getElementById("priceWindow");
let price = 0;

userPrice.addEventListener("focus", function (e) {
    e.preventDefault();
    priceWindow.style.border= `5px green solid`;
})

userPrice.addEventListener("blur", function (e) {
    if(this.value < 0){
        userPrice.style.border = `2px red solid`;
        const errorMsg = document.createElement("p");
        
        errorMsg.textContent = "Please enter correct price";
        priceWindow.after(errorMsg);
    }
    else {
        e.preventDefault();
        priceWindow.style.border = `none`;
        price = this.value;
        userPrice.style.backgroundColor = `green`;
        let span = document.createElement("span")
        span.innerHTML = `Текущая цена: ${price} <span class="close">X</span>`;
        document.body.before(span);
        span.addEventListener("click", function (e) {
            if (e.target.classList.contains("close")) {
                this.remove();
                price = 0;
                userPrice.value = "0";
                userPrice.style.backgroundColor = `white`;
            }
        })
    }
})