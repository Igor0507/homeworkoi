$(".tabs-caption li").click(function () {
    $(this).siblings().removeClass("active");
    $(this).addClass("active");
    const index = $(this).index();
    $(".tabs-content").removeClass("active");
    $(".tabs-content").eq(index).addClass("active")

})

$(".our-work-item").hide().slice(0,12).show();
$("#our-work-btn").on("click",function () {
    const selector = $(this).data("filter");
    $(`${selector}:hidden`).slice(0,12).show();
    $("#our-work-btn2").css("display", "block");
})

$("#our-work-btn2").on("click",function () {
    $(".our-work-item").hide().slice(0,12).show();
    $("#our-work-btn2").css("display","none")
})

$(".our-work-tabs li").click(function () {
    $(this).siblings().removeClass("active");
    $(this).addClass("active");
    $(".our-work-item").hide("slow");
    const selector = $(this).data("target");
    $(`.our-work-item${selector}`).show("slow");
    if($(".all").hasClass("active")){
        $("#our-work-btn").css("display","flex")
    }
    else{
        $("#our-work-btn").css("display","none")
    }
})
$('.slide-picture').slick({
    slidesToShow: 4,
    slidesToScroll: 0,
    asNavFor: '.slide-info',
    dots: false,
    centerMode:true,
    infinite: false,
    variableWidth: true,
    focusOnSelect: true
});
$('.slide-info').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slide-picture'
});
